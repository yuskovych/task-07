package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.epam.rd.java.basic.task7.db.entity.*;
import com.mysql.cj.Messages;
import com.mysql.cj.jdbc.JdbcStatement;

import javax.sql.DataSource;


public class DBManager {

    private static DBManager instance;


    public static synchronized DBManager getInstance() {
        if (instance == null) instance = new DBManager();
        return instance;
    }

    private DBManager() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public Connection getConnection() throws DBException {
        Connection connection = null;
        try {
            String connectionUrl = getConnectionUrl();
            connection = DriverManager.getConnection(connectionUrl);

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return connection;
    }

    public List<User> findAllUsers() throws DBException {

        String sql = "SELECT * FROM users";
        List<User> users = new ArrayList<>();


        try (
                Connection connection = getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(sql);
        ) {
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                users.add(user);
            }

        } catch (SQLException ignored) {

        }
        return users;


    }

    private static <T, U extends Comparable<? super U>> List<T>
    sort(List<T> items, Function<T, U> extractor) {
        items.sort(Comparator.comparing(extractor));
        return items;
    }


    public static void main(String[] args) throws DBException {
        DBManager dbm = DBManager.getInstance();
        List<User> users = IntStream.range(1, 5)
                .mapToObj(x -> "user" + x)
                .map(User::createUser)
                .collect(Collectors.toList());

        for (User user : users) {
            dbm.insertUser(user);
        }

        List<User> usersFromDB = sort(dbm.findAllUsers(), User::getLogin);
        System.out.println(usersFromDB);
        System.out.println(users);


    }


    public boolean insertUser(User user) throws DBException {
        if (user == null) return false;
        String sql = "INSERT INTO users(login) VALUES(?)";
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ) {
            preparedStatement.setString(1, user.getLogin());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) return false;

            try (ResultSet generatedKeysResult = preparedStatement.getGeneratedKeys()) {
                if (generatedKeysResult.next()) user.setId(generatedKeysResult.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }


    public boolean deleteUsers(User... users) throws DBException {
        String sql = "DELETE FROM users WHERE id = ?";
        int deletedCount = 0;
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            for (User u : users) {
                preparedStatement.setInt(1, u.getId());
                deletedCount = deletedCount + preparedStatement.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return deletedCount != 0;
    }


    public User getUser(String login) throws DBException {
        String sql = "SELECT * FROM users WHERE login=?";
        User fetchedUser = new User();
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                fetchedUser.setLogin(resultSet.getString(2));
                fetchedUser.setId(resultSet.getInt(1));
            }

        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return fetchedUser;

    }


    public Team getTeam(String name) throws DBException {
        String sql = "SELECT * FROM teams WHERE name = ?";
        Team fetchedTeam = new Team();
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                fetchedTeam.setName(resultSet.getString(2));
                fetchedTeam.setId(resultSet.getInt(1));
            }

        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return fetchedTeam;
    }

    public List<Team> findAllTeams() throws DBException {
        String sql = "SELECT * FROM teams";
        List<Team> teams = new ArrayList<>();


        try (
                Connection connection = getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(sql);
        ) {
            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt(1));
                team.setName(resultSet.getString(2));
                teams.add(team);
            }

        } catch (SQLException ignored) {

        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        if (team == null) return false;
        String sql = "INSERT INTO teams(name) VALUES(?)";
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ) {
            preparedStatement.setString(1, team.getName());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) return false;

            try (ResultSet generatedKeysResult = preparedStatement.getGeneratedKeys()) {
                if (generatedKeysResult.next()) team.setId(generatedKeysResult.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        String sql = "INSERT INTO users_teams values(?,?)";
        int affectedRows = 0;
        Connection connection = getConnection();


        try (
                PreparedStatement preparedStatement = connection.prepareStatement(sql);

        ) {
            connection.setAutoCommit(false);
            preparedStatement.setInt(1, user.getId());

            for (Team team : teams) {
                preparedStatement.setInt(2, team.getId());
                affectedRows += preparedStatement.executeUpdate();
            }
            connection.commit();

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            throw new DBException(e.getMessage(),e);

        }
        return affectedRows != 0;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        String sql =
                "SELECT * FROM teams \n" +
                        "LEFT JOIN users_teams ON users_teams.team_id = teams.id\n" +
                        "LEFT JOIN users ON users.id = users_teams.user_id\n" +
                        "WHERE users.id = ?";

        List<Team> userTeams = new ArrayList<>();
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
                userTeams.add(team);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userTeams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        String sql = "DELETE FROM teams WHERE id = ?";
        int deletedCount = 0;
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setInt(1, team.getId());
            deletedCount = preparedStatement.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return deletedCount != 0;
    }

    public boolean updateTeam(Team team) throws DBException {
        int affectedRows = 0;
        String sql = "UPDATE teams SET name=? WHERE id=?";
        try (
                Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql);

        ) {
            preparedStatement.setString(1, team.getName());
            preparedStatement.setInt(2, team.getId());
            affectedRows = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return affectedRows != 0;
    }

    private String getConnectionUrl() {
        Properties properties = new Properties();
        try (InputStream stream = Files.newInputStream(Paths.get("app.properties"))) {
            properties.load(stream);
        } catch (IOException ignored) {
        }
        return properties.getProperty("connection.url");
    }

}
